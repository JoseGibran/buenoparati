package handlers;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import models.Usuario;
import util.DB;

public class UsuarioHandler {
     
    private static ArrayList<Usuario> mUsuario = null;
       
    
    public static ArrayList<Usuario> get(){
        UsuarioHandler.set();
        return mUsuario;
}
    private static void set(){
        mUsuario = mUsuario == null ? new ArrayList<Usuario>() : mUsuario;
        mUsuario.clear();
        try {
            ResultSet result = DB.getInstance().execQuery("SELECT * FROM usuario ");
            while(result.next()){
                Usuario usuario = new Usuario(result.getInt("id_usuario"), result.getInt("id_tipo_usuario"), result.getString("nombre"), result.getString("correo"), result.getString("pass"));
                mUsuario.add(usuario);
            }
        } catch (SQLException ex) {
            Logger.getLogger(UsuarioHandler.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    //Insert
    public static boolean save(Usuario usuario){
        String sql = "INSERT INTO USUARIO(id_tipo_usuario, nombre, correo, pass) VALUES('" +
                usuario.getId_tipo_usuario() + "','" +
                usuario.getNombre() + "','" +
                usuario.getCorreo() + "','" +
                usuario.getPass() + "')"; 
        return DB.getInstance().execStatment(sql);
    }
    //Delete
    public static boolean delete(int id_usuario){
        String sql = "DELETE FROM usuario WHERE id_usuario =" + id_usuario;
        return DB.getInstance().execStatment(sql);
    }
    //Update
    public static boolean update(Usuario record){
        String sql="UPDATE usuario "
                + "SET id_tipo_usuario = '" + record.getId_tipo_usuario() + "' "
                + "SET nombre = '" + record.getNombre() + "' "
                + "SET correo = '" + record.getCorreo() + "' "
                + "SET contrasena = '" + record.getPass() + "' "
                + "WHERE id_usuario=" + record.getId_usuario() ;
        System.out.println(sql);
        return DB.getInstance().execStatment(sql);
    }
    
    //public static boolean update(int id_usuario){
        //String sql = "UPDATE usuario SET id_tipo_usuario=?, nombre=?, correo=?, contrasena=? WHERE id_usuario =" + id_usuario;
        //return DB.getInstance().execStatment(sql);
    //}
}
