package handlers;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import models.Producto;
import util.DB;

public class ProductoHandler {
    
    private static ArrayList<Producto> mProducto = null;
       
    
    public static ArrayList<Producto> get(){
        ProductoHandler.set();
        return mProducto;
}
    private static void set(){
        mProducto = mProducto == null ? new ArrayList<Producto>() : mProducto;
        mProducto.clear();
        try {
            ResultSet result = DB.getInstance().execQuery("SELECT * FROM producto ");
            while(result.next()){
                Producto producto = new Producto(result.getInt("id_producto"), result.getInt("id_categoria"), result.getString("nombre"), result.getInt("precio"));
                mProducto.add(producto);
            }
        } catch (SQLException ex) {
            Logger.getLogger(ProductoHandler.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    //Insert
    public static boolean save(Producto producto){
        String sql = "INSERT INTO PRODUCTO(nombre, precio, id_categoria) VALUES('" +
                producto.getNombre() + "','" +
                producto.getPrecio() + "','" +
                producto.getId_categoria() + "')"; 
        return DB.getInstance().execStatment(sql);
    }
    //Delete
    public static boolean delete(int id_producto){
        String sql = "DELETE FROM producto WHERE id_producto =" + id_producto;
        return DB.getInstance().execStatment(sql);
    }
    //Update
     public static boolean update(Producto record){
        String sql="UPDATE producto "
                + "SET nombre = '" + record.getNombre() + "', "
                + "SET precio = '" + record.getPrecio() + "' "
                + "WHERE id_categoria=" + record.getId_categoria() ;
        System.out.println(sql);
        return DB.getInstance().execStatment(sql);
    }
    
    //public static boolean update(int id_producto){
        //String sql = "UPDATE producto SET nombre=?, id_categoria=? WHERE id_producto =" + id_producto;
        //return DB.getInstance().execStatment(sql);
    //}
    
  }
