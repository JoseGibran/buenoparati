package handlers;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import models.Categoria;
import util.DB;

public class CategoriaHandler {
    private static ArrayList<Categoria> mCategoria = null;
       
    
    public static ArrayList<Categoria> get(){
        CategoriaHandler.set();
        return mCategoria;
}
    private static void set(){
        mCategoria = mCategoria == null ? new ArrayList<Categoria>() : mCategoria;
        mCategoria.clear();
        try {
            ResultSet result = DB.getInstance().execQuery("SELECT * FROM categoria ");
            while(result.next()){
                Categoria categoria = new Categoria(result.getInt("id_categoria"), result.getString("nombre"));
                mCategoria.add(categoria);
            }
        } catch (SQLException ex) {
            Logger.getLogger(CategoriaHandler.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    //Insert
    public static boolean save(Categoria categoria){
        String sql = "INSERT INTO CATEGORIA(nombre) VALUES('" +
                categoria.getNombre() + "')"; 
        return DB.getInstance().execStatment(sql);
    }
    //Delete
    public static boolean delete(int id_categoria){
        String sql = "DELETE FROM categoria WHERE id_categoria =" + id_categoria;
        return DB.getInstance().execStatment(sql);
    }
    //Update
    public static boolean update(Categoria record){
        String sql= "UPDATE FROM CATEGORIA" +
        "SET nombre =  " + record.getNombre() + "' " +
        "WHERE id_categoria = " + record.getId_categoria();
        System.out.println(sql);
    return DB.getInstance().execStatment(sql);
    }
    
    //public static boolean update(int id_categoria){
        //String sql = "UPDATE categoria SET nombre=? WHERE id_categoria =" + id_categoria;
        //return DB.getInstance().execStatment(sql);
    //}
   
}
