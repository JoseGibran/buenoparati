package handlers;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import models.Tipo_Usuario;
import util.DB;

public class TipoUsuarioHandler {
    private static ArrayList<Tipo_Usuario> mTipo_Usuario = null;
       
    
    public static ArrayList<Tipo_Usuario> get(){
        TipoUsuarioHandler.set();
        return mTipo_Usuario;
}
    private static void set(){
        mTipo_Usuario = mTipo_Usuario == null ? new ArrayList<Tipo_Usuario>() : mTipo_Usuario;
        mTipo_Usuario.clear();
        try {
            ResultSet result = DB.getInstance().execQuery("SELECT * FROM tipo_usuario ");
            while(result.next()){
                Tipo_Usuario tipo_usuario = new Tipo_Usuario(result.getInt("id_tipo_usuario"), result.getString("nombre"));
                mTipo_Usuario.add(tipo_usuario);
            }
        } catch (SQLException ex) {
            Logger.getLogger(TipoEstadoHandler.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    //Insert
    public static boolean save(Tipo_Usuario tipo_usuario){
        String sql = "INSERT INTO TIPO_USUARIO(nombre) VALUES('" +
                tipo_usuario.getNombre() + "')"; 
        return DB.getInstance().execStatment(sql);
    }
    //Delete
    public static boolean delete(int id_tipo_usuario){
        String sql = "DELETE FROM tipo_usuario WHERE id_tipo_usuario =" + id_tipo_usuario;
        return DB.getInstance().execStatment(sql);
    }
    //Update
    public static boolean update(Tipo_Usuario record){
        String sql="UPDATE tipo_usuario "
                + "SET nombre = '" + record.getNombre() + "' "
                + "WHERE id_tipo_usuario=" + record.getId_tipo_usuario() ;
        System.out.println(sql);
        return DB.getInstance().execStatment(sql);
    }

}
