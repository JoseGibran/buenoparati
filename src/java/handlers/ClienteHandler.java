package handlers;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import models.Cliente;
import util.DB;

public class ClienteHandler {
      
    private static ArrayList<Cliente> mCliente = null;
       
    
    public static ArrayList<Cliente> get(){
        ClienteHandler.set();
        return mCliente;
}
    private static void set(){
        mCliente = mCliente == null ? new ArrayList<Cliente>() : mCliente;
        mCliente.clear();
        try {
            ResultSet result = DB.getInstance().execQuery("SELECT * FROM cliente ");
            while(result.next()){
                Cliente cliente = new Cliente(result.getInt("id_cliente"), result.getString("nombre"), result.getInt("nit"));
                mCliente.add(cliente);
            }
        } catch (SQLException ex) {
            Logger.getLogger(ClienteHandler.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    //Insert
    public static boolean save(Cliente cliente){
        String sql = "INSERT INTO CLIENTE(nombre, nit) VALUES('" +
                cliente.getNombre() + "','" +
                cliente.getNit() + "')"; 
        return DB.getInstance().execStatment(sql);
    }
    //Delete
    public static boolean delete(int id_cliente){
        String sql = "DELETE FROM cliente WHERE id_cliente =" + id_cliente;
        return DB.getInstance().execStatment(sql);
    }
    //Update
    public static boolean update(Cliente record){
        String sql="UPDATE cliente "
                + "SET nombre = '" + record.getNombre() + "' "
                + "SET nit = '" + record.getNit() + "' "
                + "WHERE id_cliente=" + record.getId_cliente() ;
        System.out.println(sql);
        return DB.getInstance().execStatment(sql);
    }
    
    //public static boolean update(int id_cliente){
        //String sql = "UPDATE cliente SET nombre=?, nit=? WHERE id_cliente =" + id_cliente;
        //return DB.getInstance().execStatment(sql);
    //}
}
