package handlers;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import models.Tipo_Estado;
import util.DB;

public class TipoEstadoHandler {
    private static ArrayList<Tipo_Estado> mTipo_Estado = null;
       
    
    public static ArrayList<Tipo_Estado> get(){
        TipoEstadoHandler.set();
        return mTipo_Estado;
}
    private static void set(){
        mTipo_Estado = mTipo_Estado == null ? new ArrayList<Tipo_Estado>() : mTipo_Estado;
        mTipo_Estado.clear();
        try {
            ResultSet result = DB.getInstance().execQuery("SELECT * FROM tipo_estado ");
            while(result.next()){
                Tipo_Estado tipo_estado = new Tipo_Estado(result.getInt("id_tipo_estado"), result.getString("nombre"));
                mTipo_Estado.add(tipo_estado);
            }
        } catch (SQLException ex) {
            Logger.getLogger(TipoEstadoHandler.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    //Insert
    public static boolean save(Tipo_Estado tipo_estado){
        String sql = "INSERT INTO TIPO_ESTADO(nombre) VALUES('" +
                tipo_estado.getNombre() + "')"; 
        return DB.getInstance().execStatment(sql);
    }
    //Delete
    public static boolean delete(int id_tipo_estado){
        String sql = "DELETE FROM tipo_estado WHERE id_tipo_estado =" + id_tipo_estado;
        return DB.getInstance().execStatment(sql);
    }
    //Update
     public static boolean update(Tipo_Estado record){
        String sql="UPDATE tipo_estado "
                + "SET nombre = '" + record.getNombre() + "' "
                + "WHERE id_tipo_estado=" + record.getId_tipo_estado() ;
        System.out.println(sql);
        return DB.getInstance().execStatment(sql);
    }
    
    //public static boolean update(int id_tipo_estado){
        //String sql = "UPDATE tipo_estado SET nombre=? WHERE id_tipo_estado =" + id_tipo_estado;
        //return DB.getInstance().execStatment(sql);
    //}
}
