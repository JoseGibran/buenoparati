package handlers;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import models.Transaccion;
import util.DB;

public class TransaccionHandler {
    
    private static ArrayList<Transaccion> mTransaccion = null;
       
    
    public static ArrayList<Transaccion> get(){
        TransaccionHandler.set();
        return mTransaccion;
}
    private static void set(){
        mTransaccion = mTransaccion == null ? new ArrayList<Transaccion>() : mTransaccion;
        mTransaccion.clear();
        try {
            ResultSet result = DB.getInstance().execQuery("SELECT * FROM transaccion ");
            while(result.next()){
                Transaccion transaccion = new Transaccion(result.getInt("id_transaccion"), result.getInt("id_cliente"));
                mTransaccion.add(transaccion);
            }
        } catch (SQLException ex) {
            Logger.getLogger(TransaccionHandler.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    //Insert
    public static boolean save(Transaccion transaccion){
        String sql = "INSERT INTO TRANSACCION(id_cliente) VALUES('" +
                transaccion.getId_cliente() + "')"; 
        return DB.getInstance().execStatment(sql);
    }
    //Delete
    public static boolean delete(int id_transaccion){
        String sql = "DELETE FROM transaccion WHERE id_transaccion =" + id_transaccion;
        return DB.getInstance().execStatment(sql);
    }
    //Update
    public static boolean update(Transaccion record){
        String sql= "UPDATE transaccion "
                + "SET id_cliente = '" + record.getId_cliente() + "' "
                + "WHERE id_transaccion=" + record.getId_transaccion() ;
        System.out.println(sql);
        return DB.getInstance().execStatment(sql);
    }
    
    //public static boolean update(int id_transaccion){
        //String sql = "UPDATE transaccion SET id_cliente=? WHERE id_transaccion =" + id_transaccion;
        //return DB.getInstance().execStatment(sql);
    //}
}
