package handlers;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import models.Transaccion_Detalle;
import util.DB;

public class TransaccionDetalleHandler {
     
    private static ArrayList<Transaccion_Detalle> mTransaccion_Detalle = null;
       
    
    public static ArrayList<Transaccion_Detalle> get(){
        TransaccionDetalleHandler.set();
        return mTransaccion_Detalle;
}
    private static void set(){
        mTransaccion_Detalle = mTransaccion_Detalle == null ? new ArrayList<Transaccion_Detalle>() : mTransaccion_Detalle;
        mTransaccion_Detalle.clear();
        try {
            ResultSet result = DB.getInstance().execQuery("SELECT * FROM transaccion_detalle ");
            while(result.next()){
                Transaccion_Detalle transaccion_detalle = new Transaccion_Detalle(result.getInt("id_transaccion"), result.getInt("id_producto"), result.getInt("cantidad"));
                mTransaccion_Detalle.add(transaccion_detalle);
            }
        } catch (SQLException ex) {
            Logger.getLogger(TransaccionDetalleHandler.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    //Insert
    public static boolean save(Transaccion_Detalle transaccion_detalle){
        String sql = "INSERT INTO TRANSACCION_DETALLE(id_transaccion, id_producto, cantidad) VALUES('" +
                transaccion_detalle.getId_transaccion() + "','" +
                transaccion_detalle.getId_producto() + "','" +
                transaccion_detalle.getCantidad() + "')"; 
        return DB.getInstance().execStatment(sql);
    }
    //Delete
    public static boolean delete(int id_transaccion_detalle){
        String sql = "DELETE FROM transaccion_detalle WHERE id_transaccion_detalle =" + id_transaccion_detalle;
        return DB.getInstance().execStatment(sql);
    }
    //Update
     public static boolean update(Transaccion_Detalle record){
        String sql= "UPDATE transaccion_detalle "
                + "SET id_transaccion = '" + record.getId_transaccion() + "' "
                + "SET id_producto = '" + record.getId_producto() + "' "
                + "SET cantidad = '" + record.getCantidad() + "' "
                + "WHERE id_transaccion_detalle = " + record.getId_transaccion_detalle() ;
        System.out.println(sql);
        return DB.getInstance().execStatment(sql);
    }
    
    //public static boolean update(int id_transaccion_detalle){
        //String sql = "UPDATE transaccion_detalle SET id_transaccion=?, id_producto=?, cantidad=? WHERE id_transaccion_detalle =" + id_transaccion_detalle;
        //return DB.getInstance().execStatment(sql);
    //}
}
