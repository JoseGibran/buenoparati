package models;
public class Usuario {
   private int id_usuario;
   private int id_tipo_usuario;
   private String nombre;
   private String correo;
   private String pass;
 
   public Usuario(int id_usuario, int id_tipo_usuario, String nombre, String correo, String pass){
       this.id_usuario = id_usuario;
       this.id_tipo_usuario = id_tipo_usuario;
       this.nombre = nombre;
       this.correo = correo;
       this.pass = pass;
   }
   
   public Usuario(){
   
   }
           
   public void setId_usuario(int id_usuario){
       this.id_usuario = id_usuario;
   }
   public int getId_usuario(){
       return id_usuario;
   }
   
  
   public void setId_tipo_usuario(int id_tipo_usuario){
       this.id_tipo_usuario = id_tipo_usuario;
   }
   public int getId_tipo_usuario(){
       return id_tipo_usuario;
   }
   
   
   public void setNombre(String nombre){
       this.nombre = nombre;
   }
   public String getNombre(){
       return nombre;
   }
   
   public void setCorreo(String correo){
       this.correo = correo;
   }
   public String getCorreo(){
       return correo;
   }
   
   public void setPass(String contrasena){
       this.pass = pass;
   }
   public String getPass(){
       return pass;
   }
    
}
