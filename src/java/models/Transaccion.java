package models;
public class Transaccion {
   private int id_transaccion;
   private int id_cliente;
 
   public Transaccion(int id_transaccion, int id_cliente){
       this.id_transaccion = id_transaccion;
       this.id_cliente = id_cliente;
   }
           
   public void setId_transaccion(int id_transaccion){
       this.id_transaccion = id_transaccion;
   }
   public int getId_transaccion(){
       return id_transaccion;
   }
   
  
   public void setId_cliente(int id_cliente){
       this.id_cliente = id_cliente;
   }
   public int getId_cliente(){
       return id_cliente;
   }
   
}
