package models;
public class Transaccion_Detalle {
    
   private int id_transaccion_detalle;
   private int id_transaccion;
   private int id_producto;
   private int cantidad;
 
   public Transaccion_Detalle(int id_transaccion, int id_producto, int cantidad){
       this.id_transaccion = id_transaccion;
       this.id_producto = id_producto;
       this.cantidad = cantidad;
   }
   
   public void setId_transaccion_detalle(int id_transaccion_detalle){
       this.id_transaccion_detalle = id_transaccion_detalle;
   }
   public int getId_transaccion_detalle(){
       return id_transaccion_detalle;
   }
           
   public void setId_transaccion(int id_transaccion){
       this.id_transaccion = id_transaccion;
   }
   public int getId_transaccion(){
       return id_transaccion;
   }
   
  
   public void setId_producto(int id_producto){
       this.id_producto = id_producto;
   }
   public int getId_producto(){
       return id_producto;
   }
   
   
   public void setCantidad(int cantidad){
       this.cantidad = cantidad;
   }
   public int getCantidad(){
       return cantidad;
   }
}
