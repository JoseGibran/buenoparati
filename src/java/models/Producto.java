package models;
public class Producto {
   private int id_producto;
   private int id_categoria;
   private String nombre;
   private int precio;

  
 
   public Producto(int id_producto, int id_categoria, String nombre, int precio){
       this.id_producto = id_producto;
       this.id_categoria = id_categoria;
       this.nombre = nombre;
       this.precio = precio;
   }
   public Producto(){
   
   }
           
   public void setId_producto(int id_producto){
       this.id_producto = id_producto;
   }
   public int getId_producto(){
       return id_producto;
   }
   
  
   public void setId_categoria(int id_categoria){
       this.id_categoria = id_categoria;
   }
   public int getId_categoria(){
       return id_categoria;
   }
   
   
   public void setNombre(String nombre){
       this.nombre = nombre;
   }
   public String getNombre(){
       return nombre;
   }
    
    public int getPrecio() {
        return precio;
    }

    public void setPrecio(int precio) {
        this.precio = precio;
    }
}
