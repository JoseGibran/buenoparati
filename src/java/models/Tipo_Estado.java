package models;

public class Tipo_Estado {

    private int id_tipo_estado;
    private String nombre;

    public Tipo_Estado(int id_tipo_estado, String nombre) {
        this.id_tipo_estado = id_tipo_estado;
        this.nombre = nombre;
    }

    public Tipo_Estado(){
    
    }
            
    public void setId_tipo_estado(int id_tipo_estado) {
        this.id_tipo_estado = id_tipo_estado;
    }

    public int getId_tipo_estado() {
        return id_tipo_estado;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getNombre() {
        return nombre;
    }

}
