package models;
public class Cliente {
   private int id_cliente;
   private String nombre;
   private int nit;
 
   public Cliente(int id_cliente, String nombre, int nit){
       this.id_cliente = id_cliente;
       this.nombre = nombre;
       this.nit = nit;
   }
   public Cliente(){
      }
   
           
   public void setId_cliente(int id_cliente){
       this.id_cliente = id_cliente;
   }
   public int getId_cliente(){
       return id_cliente;
   }
   
  
   public void setNombre(String nombre){
       this.nombre = nombre;
   }
   public String getNombre(){
       return nombre;
   }
   
   
   public void setNit(int nit){
       this.nit = nit;
   }
   public int getNit(){
       return nit;
   }
}
