/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package util;

/**
 *
 * @author Gibran
 */

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.logging.Level;
import java.util.logging.Logger;

public class DB {
    
    private static DB instance = null;
    private Connection conn = null;
    
    Statement stmt = null;
    ResultSet rs = null;

    
    private DB(){
         try {
            Class.forName("com.mysql.jdbc.Driver").newInstance();
            conn = DriverManager.getConnection("jdbc:mysql://localhost/bueno_para_ti?user=root&password=");
        }catch (SQLException ex) {
            System.out.println("SQLException: " + ex.getMessage());
            System.out.println("SQLState: " + ex.getSQLState());
            System.out.println("VendorError: " + ex.getErrorCode());
        }catch(Exception e){
            System.out.println("ERROR: " + e.getMessage());
        }
    }
    public static DB getInstance(){
        if(instance == null){
            instance = new DB();
        }
        return instance;
    }
    
    public ResultSet execQuery(String query){
        try{
            stmt = conn.createStatement();
            rs = stmt.executeQuery(query);
            return rs;
           
        }catch(SQLException e){
            e.printStackTrace();
            System.out.println(e.getMessage());
            return null;
        }
    }
    public boolean execStatment(String statement){
        try {
            stmt = conn.createStatement();
            return stmt.execute(statement);
        } catch (SQLException ex) {
            Logger.getLogger(DB.class.getName()).log(Level.SEVERE, null, ex);
            return false;
        }
    }
    
}
