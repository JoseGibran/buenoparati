/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package util;

import java.util.ArrayList;
import models.Producto;
import models.Usuario;

/**
 *
 * @author gibran
 */
public class Context  {
    
    private static Context instance = null;
    private Usuario user;
    
    private static ArrayList<Producto> cart = null;
    
    private Context(){
        cart = new ArrayList();
    }
    public static Context getInstance(){
         if(instance == null){
            instance = new Context();
        }
        return instance;
    }
    
    public void setCurrentUser(Usuario user){
        this.user = user;
    }
    public Usuario getCurrentUser(){
        return this.user;
    }
    
    public ArrayList<Producto> getCart(){
        return this.cart;
    }
    
    public void removeFromCart(int id){
        ArrayList<Producto> list = (ArrayList<Producto>)this.cart.clone();
        for(Producto p : list){
             System.out.println( p.getId_producto() + " " + id);
            if(p.getId_producto() == id){
                 this.cart.remove(this.cart.indexOf (p));
            }
        }
    }
    
      
}
