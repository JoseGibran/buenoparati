/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controllers;

import handlers.ProductoHandler;
import java.sql.ResultSet;
import java.sql.SQLException;
import models.Usuario;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import util.DB;
import util.Context;

/**
 *
 * @author gibran
 */

@Controller
public class LoginController {
    
    
     @RequestMapping("/login")
   // @ResponseBody //FOR WEB_API
    public String home(ModelMap modelMap) {
        return "login/login";
    }
     @RequestMapping(method = RequestMethod.POST, value = "/login")
    public String login(
            ModelMap modelMap,
            @RequestParam("correo") String correo,
            @RequestParam("pass") String pass
    ) throws SQLException{
        
        ResultSet result = DB.getInstance().execQuery("SELECT * FROM usuario");
        while(result.next()){
            if(correo.equals(result.getString("correo"))){
                if(pass.equals(result.getString("pass"))){
                    Usuario usuario = new Usuario(result.getInt("id_usuario"), result.getInt("id_tipo_usuario"), result.getString("nombre"), result.getString("correo"), result.getString("pass"));
                    Context.getInstance().setCurrentUser(usuario);
                    
                    modelMap.put("products", ProductoHandler.get());
                    
                    return "index";
                }
            }
        }
        modelMap.put("login", false);
        return "login/login";
    }
    
}
