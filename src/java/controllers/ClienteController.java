
package controllers;

import handlers.ClienteHandler;
import models.Cliente;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;



@Controller
public class ClienteController {
    
     @RequestMapping("/cliente")
    public String list(ModelMap modelMap) {
        modelMap.put("list", ClienteHandler.get());
        return "cliente/index";
    }
    
    @RequestMapping("/cliente/form")
    public String create(ModelMap modelMap) {
      
        return "cliente/form";
    }
    
    @RequestMapping(method = RequestMethod.POST, value = "/cliente")
    public String store(
        ModelMap modelMap,
        @RequestParam("nombre") String nombre,
        @RequestParam("nit") String nit
        
    ){
        Cliente nuevoCliente = new Cliente(0, nombre,Integer.parseInt(nit));
        ClienteHandler.save(nuevoCliente);
        
        return this.list(modelMap);
    }
    
     @RequestMapping(method = RequestMethod.GET, value="/cliente/edit/{id}")
    public String edit(ModelMap modelMap, @PathVariable(value ="id") String id){
        Cliente cliente = null;
        for(Cliente x : ClienteHandler.get()){
            if(x.getId_cliente() == Integer.parseInt(id)){
                cliente = x;
            }
        }
        modelMap.put("cliente", cliente);
        
        return "cliente/form";
    }
    
    @RequestMapping(method = RequestMethod.POST, value="/cliente/update/{id}")
    public String update(
            ModelMap modelMap, 
            @PathVariable(value ="id") String id,
            @RequestParam("nombre") String nombre,
            @RequestParam("nit") String nit
            
            ){
        Cliente cliente = new Cliente();
        cliente.setNombre(nombre);
        cliente.setNit(Integer.parseInt(nit));
        
       ClienteHandler.update(cliente);
        
        return this.list(modelMap);
    }
    
     @RequestMapping(method = RequestMethod.POST, value = "/cliente/delete" )
    public String delete(
        ModelMap modelMap,
        @RequestParam("id") String id
    ){
        Cliente cliente = new Cliente();
        cliente.setId_cliente(Integer.parseInt(id));
        ClienteHandler.delete(cliente.getId_cliente());
        return this.list(modelMap);
    }
  
    
}
