/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controllers;

import handlers.ClienteHandler;
import handlers.ProductoHandler;
import handlers.TransaccionDetalleHandler;
import handlers.TransaccionHandler;
import models.Cliente;
import models.Producto;
import models.Transaccion;
import models.Transaccion_Detalle;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import util.Context;
import util.DB;

/**
 *
 * @author Gibran
 */
@Controller
public class HomeController {
    @RequestMapping("/")
   // @ResponseBody //FOR WEB_API
    public String home(ModelMap modelMap) {
        modelMap.put("products", ProductoHandler.get());
        
        return Context.getInstance().getCurrentUser() != null ? "index" : "login/login";

    }
     @RequestMapping("/cart")
    public String cart(ModelMap modelMap){
        
        int total = 0;
        for(Producto i : Context.getInstance().getCart()){
            total += i.getPrecio();
        }
        
        modelMap.put("total", total);
        modelMap.put("cart", Context.getInstance().getCart()) ;
        return "commerce/cart";
    }
    @RequestMapping("/pos")
    public String pos(ModelMap modelMap){
        return "commerce/pos";
    }
    @RequestMapping(method = RequestMethod.POST, value= "/finish")
    public String finish(ModelMap modelMap, @RequestParam("nit") String nit){
       
        Cliente newCliente = new Cliente();
        newCliente.setNombre("CF");
        newCliente.setNit(Integer.parseInt(nit));
        newCliente.setId_cliente(0);
        
        ClienteHandler.save(newCliente);
        
        Transaccion transaccion = new Transaccion(0, ClienteHandler.get().size());
        TransaccionHandler.save(transaccion);
        transaccion.setId_transaccion(TransaccionHandler.get().size());
        
        
        for(Producto p : Context.getInstance().getCart()){
            Transaccion_Detalle d = new Transaccion_Detalle(transaccion.getId_transaccion(), p.getId_producto(), 1 );
            TransaccionDetalleHandler.save(d);
        }
        
        
        
        return "commerce/finish";
    }
    
    
   @RequestMapping(method = RequestMethod.GET, value = "/cart/add/{id}")
    public String addToCart(ModelMap modelMap, @PathVariable( value = "id") String id){
  
        for(Producto product : ProductoHandler.get()){
            if(product.getId_producto() == Integer.parseInt(id)){
                Context.getInstance().getCart().add(product);
            }
        }
   
        return this.home(modelMap);
    }
    
    @RequestMapping(method = RequestMethod.GET, value = "/cart/remove/{id}")
    public String removeFromCart(ModelMap modelMap, @PathVariable(value = "id") String id){
        Context.getInstance().removeFromCart(Integer.parseInt(id));
        return this.cart(modelMap);
    }
    
}
