/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controllers;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import util.DB;
/**
 *
 * @author Gibran
 */
@Controller
public class TransaccionController {
    
    @RequestMapping("/transaccion")
    public String transacciones(ModelMap modelMap) throws SQLException{
        
        String query = "SELECT c.nit, t.id_transaccion FROM transaccion t INNER JOIN cliente c ON t.id_cliente = c.id_cliente";
        
        ArrayList<HashMap<String, String>> list = new ArrayList<HashMap<String, String>>();
        
        ResultSet result = DB.getInstance().execQuery(query);
        while(result.next()){
            HashMap<String, String> record = new HashMap<String, String>();
            record.put("nit", result.getString("nit"));
            record.put("id_transaccion", result.getString("id_transaccion"));
            list.add(record);
        }
        
        System.out.println(list.size());
        modelMap.put("list", list);
        
        
        return "transaccion/index";
    }
    
    @RequestMapping("/transaccion/{id}")
    public String detail(ModelMap modelMap, @PathVariable( value = "id") String id) throws SQLException{
        
        String query = "SELECT p.nombre as producto from transaccion_detalle t INNER JOIN producto p ON t.id_producto = p.id_producto WHERE t.id_transaccion = " + id;
         ResultSet result = DB.getInstance().execQuery(query);
         ArrayList<HashMap<String, String>> list = new ArrayList<HashMap<String, String>>();
         while(result.next()){
            HashMap<String, String> record = new HashMap<String, String>();
            record.put("producto", result.getString("producto"));
            list.add(record);
        }
         System.out.println(list);
         modelMap.put("list", list);
        return "transaccion/detail";
    }
}
