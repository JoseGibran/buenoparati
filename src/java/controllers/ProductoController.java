
package controllers;
import handlers.ProductoHandler;
import models.Producto;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
public class ProductoController {
  
    @RequestMapping("/producto")
    public String list(ModelMap modelMap) {
        modelMap.put("list", ProductoHandler.get());
        return "producto/index";
    }
    
    @RequestMapping("/producto/form")
    public String create(ModelMap modelMap) {

        return "producto/form";
    }
    
   @RequestMapping(method = RequestMethod.POST, value = "/producto")
    public String store(
            ModelMap modelMap,
            @RequestParam("id_categoria") String id_categoria,
            @RequestParam("nombre") String nombre,
            @RequestParam("precio") String precio
            
    ) {
        Producto nuevoProducto = new Producto(0,Integer.parseInt(id_categoria),nombre, Integer.parseInt(precio));
        ProductoHandler.save(nuevoProducto);

        return this.list(modelMap);
    }
    
    
     @RequestMapping(method = RequestMethod.GET, value = "/producto/edit/{id}")
    public String edit(ModelMap modelMap, @PathVariable(value = "id") String id) {
        Producto producto = null;
        for (Producto x : ProductoHandler.get()) {
            if (x.getId_producto() == Integer.parseInt(id)) {
                producto = x;
            }
        }
        modelMap.put("producto", producto);

        return "producto/form";
    }
    
    @RequestMapping(method = RequestMethod.POST, value = "/producto/update/{id}")
    public String update(
            ModelMap modelMap,
            @PathVariable(value = "id") String id,
            @RequestParam("id_categoria") String id_categoria,
            @RequestParam("nombre") String nombre,
            @RequestParam("precio") String precio
           
    ) {
        Producto producto = new Producto();
        producto.setId_producto(Integer.parseInt(id));
        producto.setNombre(nombre);
        producto.setId_categoria(Integer.parseInt(id_categoria));
        producto.setPrecio(Integer.parseInt(precio));
         
        ProductoHandler.update(producto);

        return this.list(modelMap);
    }
    
     @RequestMapping(method = RequestMethod.POST, value = "/producto/delete" )
    public String delete(
        ModelMap modelMap,
        @RequestParam("id") String id
    ){
        Producto producto = new Producto();
        producto.setId_producto(Integer.parseInt(id));
        ProductoHandler.delete(producto.getId_producto());
        return this.list(modelMap);
    }
    
}

