/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controllers;

import handlers.TipoUsuarioHandler;
import models.Tipo_Usuario;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import util.DB;

/**
 *
 * @author Gibran
 */
@Controller

public class TipoUsuarioController {
    
   @RequestMapping("/tipoUsuario")
    public String list(ModelMap modelMap) {

        modelMap.put("list", TipoUsuarioHandler.get());
        
        return "tipo_usuario/index";
    }
    
   @RequestMapping("/tipoUsuario/form")
    public String create(ModelMap modelMap) {
        DB.getInstance();
        return "tipo_usuario/form";
    }
    
    
    @RequestMapping(method = RequestMethod.POST, value = "/tipoUsuario")
    public String store(
        ModelMap modelMap,
        @RequestParam("nombre") String nombre
    ){
        Tipo_Usuario nuevoTipoUsuario = new Tipo_Usuario(0, nombre);
        TipoUsuarioHandler.save(nuevoTipoUsuario);
        
        return this.list(modelMap);
    }
    
    @RequestMapping(method = RequestMethod.GET, value="/tipoUsuario/edit/{id}")
    public String edit(ModelMap modelMap, @PathVariable(value ="id") String id){
        Tipo_Usuario tipoUsuario = null;
        for(Tipo_Usuario x : TipoUsuarioHandler.get()){
            if(x.getId_tipo_usuario() == Integer.parseInt(id)){
                tipoUsuario = x;
            }
        }
        modelMap.put("tipoUsuario", tipoUsuario);
        
        return "tipo_usuario/form";
    }
    
    @RequestMapping(method = RequestMethod.POST, value="/tipoUsuario/update/{id}")
    public String update(
            ModelMap modelMap, 
            @PathVariable(value ="id") String id,
            @RequestParam("nombre") String nombre
            ){
        Tipo_Usuario tipoUsuario = new Tipo_Usuario();
        tipoUsuario.setId_tipo_usuario(Integer.parseInt(id));
        tipoUsuario.setNombre(nombre);
        
       TipoUsuarioHandler.update(tipoUsuario);
        
        return this.list(modelMap);
    }
    
    @RequestMapping(method = RequestMethod.POST, value = "/tipoUsuario/delete" )
    public String delete(
        ModelMap modelMap,
        @RequestParam("id") String id
    ){
        Tipo_Usuario tipoUsuario = new Tipo_Usuario();
        tipoUsuario.setId_tipo_usuario(Integer.parseInt(id));
        TipoUsuarioHandler.delete(tipoUsuario.getId_tipo_usuario());
        return this.list(modelMap);
    }
    
}
