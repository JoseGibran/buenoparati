
package controllers;

import handlers.UsuarioHandler;
import models.Usuario;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;


@Controller
public class UsuarioController {

    @RequestMapping("/usuario")
    public String list(ModelMap modelMap) {
        modelMap.put("list", UsuarioHandler.get());
        return "usuario/index";
    }

    @RequestMapping("/usuario/form")
    public String create(ModelMap modelMap) {

        return "usuario/form";
    }

    @RequestMapping(method = RequestMethod.POST, value = "/usuario")
    public String store(
            ModelMap modelMap,
            @RequestParam("tipo_usuario") String tipo_usuario,
            @RequestParam("nombre") String nombre,
            @RequestParam("correo") String correo,
            @RequestParam("pass") String pass
    ) {
        Usuario nuevoUsuario = new Usuario(0, Integer.parseInt(tipo_usuario), nombre, correo, pass);
        UsuarioHandler.save(nuevoUsuario);

        return this.list(modelMap);
    }

    @RequestMapping(method = RequestMethod.GET, value = "/usuario/edit/{id}")
    public String edit(ModelMap modelMap, @PathVariable(value = "id") String id) {
        Usuario usuario = null;
        for (Usuario x : UsuarioHandler.get()) {
            if (x.getId_usuario() == Integer.parseInt(id)) {
                usuario = x;
            }
        }
        modelMap.put("usuario", usuario);

        return "usuario/form";
    }

    @RequestMapping(method = RequestMethod.POST, value = "/usuario/update/{id}")
    public String update(
            ModelMap modelMap,
            @PathVariable(value = "id") String id,
            @RequestParam("nombre") String nombre,
            @RequestParam("tipo_usuario") String tipo_usuario,
            @RequestParam("correo") String correo,
            @RequestParam("pass") String pass
    ) {
        Usuario usuario = new Usuario();
        usuario.setId_tipo_usuario(Integer.parseInt(id));
        usuario.setNombre(nombre);
        usuario.setId_tipo_usuario(Integer.parseInt(tipo_usuario));
        usuario.setCorreo(correo);
        usuario.setPass(pass);
        
        UsuarioHandler.update(usuario);

        return this.list(modelMap);
    }
    
     
    @RequestMapping(method = RequestMethod.POST, value = "/usuario/delete" )
    public String delete(
        ModelMap modelMap,
        @RequestParam("id") String id
    ){
        Usuario usuario = new Usuario();
        usuario.setId_tipo_usuario(Integer.parseInt(id));
        UsuarioHandler.delete(usuario.getId_tipo_usuario());
        return this.list(modelMap);
    }
    
}
