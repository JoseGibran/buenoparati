package controllers;

import handlers.CategoriaHandler;
import models.Categoria;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

@Controller
public class CategoriaController {

     @RequestMapping("/categoria")
    public String list(ModelMap modelMap) {

       modelMap.put("list", CategoriaHandler.get());
        
        return "categoria/index";
    }
    
     @RequestMapping("/categoria/form")
    public String create(ModelMap modelMap) {
        return "categoria/form";
    }
   
    @RequestMapping(method = RequestMethod.POST, value = "/categoria")
    public String store(
        ModelMap modelMap,
        @RequestParam("nombre") String nombre
    ){
        Categoria nuevoCategoria = new Categoria(0, nombre);
        CategoriaHandler.save(nuevoCategoria);
        
        return this.list(modelMap);
    }
    
     @RequestMapping(method = RequestMethod.GET, value="/categoria/edit/{id}")
    public String edit(ModelMap modelMap, @PathVariable(value ="id") String id){
        Categoria categoria = null;
        for(Categoria x : CategoriaHandler.get()){
            if(x.getId_categoria() == Integer.parseInt(id)){
                categoria = x;
            }
        }
        modelMap.put("categoria", categoria);
        
        return "categoria/form";
    }
    
    @RequestMapping(method = RequestMethod.POST, value="/categoria/update/{id}")
    public String update(
            ModelMap modelMap, 
            @PathVariable(value ="id") String id,
            @RequestParam("nombre") String nombre
            ){
        Categoria categoria = new Categoria();
        categoria.setId_categoria(Integer.parseInt(id));
        categoria.setNombre(nombre);
        
        CategoriaHandler.update(categoria);
        
        return this.list(modelMap);
    }
    
     @RequestMapping(method = RequestMethod.POST, value = "/categoria/delete" )
    public String delete(
        ModelMap modelMap,
        @RequestParam("id") String id
    ){
        Categoria categoria = new Categoria();
        categoria.setId_categoria(Integer.parseInt(id));
        CategoriaHandler.delete(categoria.getId_categoria());
        return this.list(modelMap);
    }
   
    
}
