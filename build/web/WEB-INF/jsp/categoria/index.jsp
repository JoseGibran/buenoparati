<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<jsp:include page="../shared/header.jsp" /> 
<div class="container">
            <div class="row">
                <div class="col-md-12">
                    <h2>Categoria</h2>
                    <a href="/buenoparati/categoria/form" class="btn btn-primary pull-right ">Crear</a>
                    <hr/>
                </div>
                <div class="col-md-12">
                    <table class="table table-hover">
                        <thead>
                            <th>ID</th>
                            <th>Nombre</th>
                            
                        </thead>
                        <tbody>
                            <c:forEach var="item" items="${list}">
                               <tr>
                                    <td>${item.getId_categoria()}</td>
                                    <td>${item.getNombre()}</td>
                                    <td style="display: flex;">
                                        <!-- EDIT -->
                                        <a class="btn btn-warning btn-sm" href="/buenoparati/categoria/edit/${item.getId_categoria()}">Editar</a>
                                        <!--DELETE -->
                                        <form method="POST" action="/buenoparati/categoria/delete">
                                            <input name="id" type="hidden" value="${item.getId_categoria()}"/>
                                            <input type="submit" class="btn btn-danger btn-sm" value="Eliminar">
                                        </form>
                                    </td>
                                </tr> 
                            </c:forEach>
                            
                        </tbody>
                    </table>
                    
                    <c:if test="${list.size() == 0}">
                         <div class="alert alert-warning">
                            No Existen Registros
                        </div>
                    </c:if>
                   
                </div>
                
                
            </div>
        </div>
<jsp:include page="../shared/footer.jsp" /> 