
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<jsp:include page="../shared/header.jsp" />
<div class="container">
    <div class="row">
        <div class="col-md-12">
            <c:if test="${categoria == null}"><h2>Crear Categoria</h2></c:if>
            <c:if test="${categoria != null}"><h2>Editar Categoria</h2></c:if>
            <hr/>
        </div>

        <div class="col-md-12">

            <c:if test="${categoria == null}">
                <form method="POST" action="/buenoparati/categoria" class="form">
                    <div class="form-group">
                        <label for="nombre" class="control-label">Nombre: </label>
                        <input name="nombre" value="" id="nombre" type="text" class="form-control" />
                    </div>
                    <div class="form-group">
                        <input class="btn btn-primary pull-right" type="submit" value="Guardar" />
                    </div>
                </form>
            </c:if>

            <c:if test="${categoria != null}">
               <form method="POST" action="/buenoparati/categoria/update/${categoria.getId_categoria()}" class="form">
                    <div class="form-group">
                        <label for="nombre" class="control-label">Nombre: </label>
                        <input name="nombre" value="${categoria.getNombre()}" id="nombre" type="text" class="form-control" />
                    </div>
                    <div class="form-group">
                        <input class="btn btn-primary pull-right" type="submit" value="Guardar" />
                    </div>
                </form>  
            </c:if> 
        </div>
    </div>
</div>
<jsp:include page="../shared/footer.jsp" /> 