 <div class="overlay"></div>
    
<nav class="navbar navbar-inverse navbar-fixed-top" id="sidebar-wrapper" role="navigation">
    <ul class="nav sidebar-nav">
        <li class="sidebar-brand">
            <a href="#">
               Bueno Para Ti
            </a>
        </li>
         <li>
            <a href="/buenoparati/">Home</a>
        </li>
        <li>
            <a href="/buenoparati/tipoUsuario">Tipos de Usuario</a>
        </li>
        <li>
            <a href="/buenoparati/cliente">Clientes</a>
        </li>
        <li>
            <a href="/buenoparati/categoria">Categorias</a>
        </li>
        <li>
            <a href="/buenoparati/producto">Productos</a>
        </li>
        <li>
            <a href="/buenoparati/usuario">Usuarios</a>
        </li>
        <li>
            <a href="/buenoparati/transaccion">Transacciones</a>
        </li>
        
        <li>
            <a href="/buenoparati/login">SALIR</a>
        </li>
    </ul>
</nav>