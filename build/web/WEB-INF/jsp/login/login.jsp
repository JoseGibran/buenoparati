    <%-- 
    Document   : login
    Created on : Nov 8, 2017, 5:42:57 PM
    Author     : gibran
--%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
         <link href="http://maxcdn.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css" rel="stylesheet">
        <link href='http://fonts.googleapis.com/css?family=Roboto:400,700,300' rel='stylesheet' type='text/css'>
        <link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">        
        <style>
               @CHARSET "UTF-8";
                /*
                over-ride "Weak" message, show font in dark grey
                */

                .progress-bar {
                    color: #333;
                } 

                /*
                Reference:
                http://www.bootstrapzen.com/item/135/simple-login-form-logo/
                */

                * {
                    -webkit-box-sizing: border-box;
                           -moz-box-sizing: border-box;
                                box-sizing: border-box;
                        outline: none;
                }

                    .form-control {
                          position: relative;
                          font-size: 16px;
                          height: auto;
                          padding: 10px;
                                @include box-sizing(border-box);

                                &:focus {
                                  z-index: 2;
                                }
                        }

                body {
                        background: url(http://i.imgur.com/GHr12sH.jpg) no-repeat center center fixed;
                    -webkit-background-size: cover;
                    -moz-background-size: cover;
                    -o-background-size: cover;
                    background-size: cover;
                }

                .login-form {
                        margin-top: 60px;
                }

                form[role=login] {
                        color: #5d5d5d;
                        background: #f2f2f2;
                        padding: 26px;
                        border-radius: 10px;
                        -moz-border-radius: 10px;
                        -webkit-border-radius: 10px;
                }
                        form[role=login] img {
                                display: block;
                                margin: 0 auto;
                                margin-bottom: 35px;
                        }
                        form[role=login] input,
                        form[role=login] button {
                                font-size: 18px;
                                margin: 16px 0;
                        }
                        form[role=login] > div {
                                text-align: center;
                        }

                .form-links {
                        text-align: center;
                        margin-top: 1em;
                        margin-bottom: 50px;
                }
                        .form-links a {
                                color: #fff;
                        }
        </style>
        
    </head>
    <body>
       <div class="container">
  
  <div class="row" id="pwd-container">
    <div class="col-md-4"></div>
    
    <div class="col-md-4">
      <section class="login-form">
        <form method="post" action="/buenoparati/login" role="login">
            <h2 style="text-align: center">Bueno Para Ti</h2>
          <input type="email" name="correo" placeholder="Email" required class="form-control input-lg" value="" />
          
          <input type="password" name="pass" class="form-control input-lg" id="pass" placeholder="Password" required="" />
          
          
          <div class="pwstrength_viewport_progress"></div>
          
          
          <button type="submit" name="go" class="btn btn-lg btn-primary btn-block">Sign in</button>
          <div>
            <a href="#">Create account</a> or <a href="#">reset password</a>
          </div>
          
        </form>
        
        <div class="form-links">
          <a href="#">www.website.com</a>
        </div>
      </section>  
      </div>
      
      <div class="col-md-4"></div>
      
      <c:if test="${login == false}">
          <div class="alert alert-danger">Usuario o Pass Incorrecto, porfavor ingrese nuevamente
          </div>
      </c:if>
      

  </div>
  
  <p>
    <a href="http://validator.w3.org/check?uri=http%3A%2F%2Fbootsnipp.com%2Fiframe%2FW00op" target="_blank"><small>HTML</small><sup>5</sup></a>
    <br>
    <br>
    
  </p>     
  
  
</div>
        
        
    </body>
</html>
