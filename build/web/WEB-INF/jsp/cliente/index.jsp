<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<jsp:include page="../shared/header.jsp" /> 
<div class="container">
            <div class="row">
                <div class="col-md-12">
                    <h2>Cliente</h2>
                    <hr/>
                </div>
                <div class="col-md-12">
                    <table class="table table-hover">
                        <thead>
                            <th>ID</th>
                            <th>Nombre</th>
                            <th>Nit</th>
                        </thead>
                        <tbody>
                            <c:forEach var="item" items="${list}">
                               <tr>
                                    <td>${item.getId_cliente()}</td>
                                    <td>${item.getNombre()}</td>
                                    <td>${item.getNit()}</td>
                                    
                                    <td style="display: flex;">
                                        <!-- EDIT -->
                                        <a class="btn btn-warning btn-sm" href="/buenoparati/cliente/edit/${item.getId_cliente()}">Editar</a>
                                        <!--DELETE -->
                                        <form method="POST" action="/buenoparati/cliente/delete">
                                            <input name="id" type="hidden" value="${item.getId_cliente()}"/>
                                            <input type="submit" class="btn btn-danger btn-sm" value="Eliminar">
                                        </form>
                                    </td>
                                </tr> 
                            </c:forEach>
                            
                        </tbody>
                    </table>
                    
                    <c:if test="${list.size() == 0}">
                         <div class="alert alert-warning">
                            No Existen Registros
                        </div>
                    </c:if>
                   
                </div>
                
                <a href="/buenoparati/cliente/form" class="btn btn-primary pull-right ">Crear</a>
            </div>
        </div>
<jsp:include page="../shared/footer.jsp" /> 