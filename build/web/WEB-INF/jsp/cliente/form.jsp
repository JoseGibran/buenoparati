
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<jsp:include page="../shared/header.jsp" />
<div class="container">
    <div class="row">
        <div class="col-md-12">
            <c:if test="${cliente == null}"><h2>Crear un Cliente</h2> </c:if>
            <c:if test="${cliente != null}"><h2>Editar Cliente</h2> </c:if>
            
            <hr/>
        </div>

        <div class="col-md-12">

            <c:if test="${cliente == null}">
                <form method="POST" action="/buenoparati/cliente" class="form">
                    <div class="form-group">
                        
                        <label for="nombre" class="control-label">Nombre: </label>
                        <input name="nombre" value="" id="nombre" type="text" class="form-control" />
                        
                        <label for="nit" class="control-label">Nit: </label>
                        <input name="nit" value="" id="nombre" type="text" class="form-control" />
                        
                    </div>
                    <div class="form-group">
                        <input class="btn btn-primary pull-right" type="submit" value="Guardar" />
                    </div>
                </form>
            </c:if>

            <c:if test="${cliente != null}">
               <form method="POST" action="/buenoparati/cliente/update/${cliente.getId_cliente()}" class="form">
                    <div class="form-group">
                        
                        <label for="nombre" class="control-label">Nombre: </label>
                        <input name="nombre" value="${cliente.getNombre()}" id="nombre" type="text" class="form-control" />
                    
                        <label for="nit" class="control-label">NIT: </label>
                        <input name="nit" value="${cliente.getNit()}" id="nombre" type="text" class="form-control" />

                    </div>
                    <div class="form-group">
                        <input class="btn btn-primary pull-right" type="submit" value="Guardar" />
                    </div>
                </form>  
            </c:if> 
        </div>
    </div>
</div>
<jsp:include page="../shared/footer.jsp" /> 