<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<jsp:include page="../shared/header.jsp" />
<div class="container">
    <div class="row">
        <div class="col-md-12">
            <c:if test="${producto == null}"> <h2>Crear Producto</h2></c:if> 
            <c:if test="${producto != null}"> <h2>Editar Producto</h2></c:if>
            
            <hr/>
        </div>

        <div class="col-md-12">

            <c:if test="${producto == null}">
                <form method="POST" action="/buenoparati/producto" class="form">
                    <div class="form-group">

                        <label for="id_categoria" class="control-label">Categoria: </label>
                        <input name="id_categoria" value="" id="nombre" type="text" class="form-control" />
                        
                        <label for="nombre" class="control-label">Nombre: </label>
                        <input name="nombre" value="" id="nombre" type="text" class="form-control" />
                        
                        <label for="precio" class="control-label">Precio:  </label>
                        <input name="precio" value="" id="precio" type="text" class="form-control" />
                    </div>
                    <div class="form-group">
                        <input class="btn btn-primary pull-right" type="submit" value="Guardar" />
                    </div>
                </form>
            </c:if>

            <c:if test="${producto != null}">
                <form method="POST" action="/buenoparati/producto/update/${producto.getId_producto()}" class="form">
                    <div class="form-group">

                        <label for="nombre" class="control-label">Nombre: </label>
                        <input name="nombre" value="${producto.getNombre()}" id="nombre" type="text" class="form-control" />

                        <label for="precio" class="control-label">Precio </label>
                        <input name="precio" value="${producto.getPrecio()}" id="precio" type="text" class="form-control" />
                        
                        <label for="id_categoria" class="control-label">Categoria: </label>
                        <input name="id_categoria" value="${producto.getId_categoria()}" id="id_categoria" type="text" class="form-control" />
                        

                    </div>
                    <div class="form-group">
                        <input class="btn btn-primary pull-right" type="submit" value="Guardar" />
                    </div>
                </form>  
            </c:if> 
        </div>
    </div>
</div>
<jsp:include page="../shared/footer.jsp" /> 