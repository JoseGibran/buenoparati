    

    </div>
</div>        
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
        <script
            src="https://code.jquery.com/jquery-3.2.1.min.js"
            integrity="sha256-hwg4gsxgFZhOsEEamdOYGBf13FyQuiTwlAQgxVSNgt4="
            crossorigin="anonymous"></script>
        <script>
            $(document).ready(function () {
                var trigger = $('.hamburger'),
                    overlay = $('.overlay'),
                   isClosed = false;

                  trigger.click(function () {
                    hamburger_cross();      
                  });

                  function hamburger_cross() {

                    if (isClosed == true) {          
                      overlay.hide();
                      trigger.removeClass('is-open');
                      trigger.addClass('is-closed');
                      isClosed = false;
                    } else {   
                      overlay.show();
                      trigger.removeClass('is-closed');
                      trigger.addClass('is-open');
                      isClosed = true;
                    }
                }

                $('[data-toggle="offcanvas"]').click(function () {
                      $('#wrapper').toggleClass('toggled');
                });  
              });
        </script>

    </body>
</html>
