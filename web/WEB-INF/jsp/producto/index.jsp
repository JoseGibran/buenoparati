<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<jsp:include page="../shared/header.jsp" /> 
<div class="container">
    <div class="row">
        <div class="col-md-12">
            <h2>Producto</h2>
        </div>
        <div class="col-md-12">
            <table class="table table-hover">
                  <a href="/buenoparati/producto/form" class="btn btn-primary pull-right ">Crear</a>
                <thead>
                <th>ID</th>
                <th>Categoria</th>
                <th>Nombre</th>
                <th>Precio</th>
             
                </thead>
                <tbody>
                    <c:forEach var="item" items="${list}">
                        <tr>
                            <td>${item.getId_producto()}</td>
                            <td>${item.getId_categoria()}</td>
                            <td>${item.getNombre()}</td>
                            <td>${item.getPrecio()}</td>
                            
                            <td style="display: flex;">
                                <!-- EDIT -->
                                <a class="btn btn-warning btn-sm" href="/buenoparati/producto/edit/${item.getId_producto()}">Editar</a>
                                <!--DELETE -->
                                <form method="POST" action="/buenoparati/producto/delete">
                                    <input name="id" type="hidden" value="${item.getId_producto()}"/>
                                    <input type="submit" class="btn btn-danger btn-sm" value="Eliminar">
                                </form>
                            </td>
                        </tr> 
                    </c:forEach>

                </tbody>
            </table>

            <c:if test="${list.size() == 0}">
                <div class="alert alert-warning">
                    No Existen Registros
                </div>
            </c:if>

        </div>
      
    </div>
</div>
<jsp:include page="../shared/footer.jsp" /> 