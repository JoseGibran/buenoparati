<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<jsp:include page="../shared/header.jsp" /> 


<div class="container">
    <div class="jumbotron text-xs-center">
        <h1 class="display-3">Thank You!</h1>
        <p class="lead"><strong>Please check your email</strong> for further instructions on how to complete your account setup.</p>
        <hr>
        <p>
          Having trouble? <a href="">Contact us</a>
        </p>
        <p class="lead">
          <a class="btn btn-primary btn-sm" href="/buenoparati/" role="button">Continue to homepage</a>
        </p>
      </div>
</div>

<jsp:include page="../shared/footer.jsp" /> 