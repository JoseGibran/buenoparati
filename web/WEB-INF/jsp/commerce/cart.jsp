<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<jsp:include page="../shared/header.jsp" /> 
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<div class="container">
    <div class="row">
        <div class="col-md-12">
            <h2>Carrito</h2>
        </div>
        
        <table class="table table-hover">
                <thead>
                <th>ID</th>
                <th>Categoria</th>
                <th>Nombre</th>
                </thead>
                
                <tbody>
                    <c:forEach var="item" items="${cart}">
                        <tr>
                            <td>${item.getId_producto()}</td>
                            <td>${item.getId_categoria()}</td>
                            <td>${item.getNombre()}</td>
                            <td>
                                <a href="/buenoparati/cart/remove/${item.getId_producto()}" class="btn btn-danger btn-sm">Eliminar</a>
                            </td>
                        </tr> 
                    </c:forEach>

                </tbody>
            </table>
        
      
    </div>
     <div class="row">
            <div class="col-md-6">
                <h2>Total: ${total}</h2>
            </div>
            <div class="col-md-6">
                <a href="/buenoparati/pos" class="btn btn-success pull-right">Finalizar</a>
            </div>
        </div>
</div>
<jsp:include page="../shared/footer.jsp" /> 