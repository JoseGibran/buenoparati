<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<jsp:include page="../shared/header.jsp" />
<div class="container">
    <div class="row">
        <div class="col-md-12">
            <c:if test="${usuario == null}"> <h2>Crear un Usuario</h2></c:if> 
            <c:if test="${usuario != null}"> <h2>Editar Usuario</h2></c:if>
            
            <hr/>
        </div>

        <div class="col-md-12">

            <c:if test="${usuario == null}">
                <form method="POST" action="/buenoparati/usuario" class="form">
                    <div class="form-group">
                        <label for="nombre" class="control-label">Nombre: </label>
                        <input name="nombre" value="" id="nombre" type="text" class="form-control" />

                        <label for="tipo_usuario" class="control-label">Tipo Usuario: </label>
                        <input name="tipo_usuario" value="" id="nombre" type="text" class="form-control" />

                        <label for="correo" class="control-label">Correo: </label>
                        <input name="correo" value="" id="nombre" type="text" class="form-control" />

                        <label for="pass" class="control-label">Contras�a: </label>
                        <input name="pass" value="" id="nombre" type="text" class="form-control" />

                    </div>
                    <div class="form-group">
                        <input class="btn btn-primary pull-right" type="submit" value="Guardar" />
                    </div>
                </form>
            </c:if>

            <c:if test="${usuario != null}">
                <form method="POST" action="/buenoparati/usuario/update/${usuario.getId_usuario()}" class="form">
                    <div class="form-group">
                        <label for="nombre" class="control-label">Nombre: </label>
                        <input name="nombre" value="${usuario.getNombre()}" id="nombre" type="text" class="form-control" />

                        <label for="tipo_usuario" class="control-label">Tipo Usuario: </label>
                        <input name="tipo_usuario" value="${usuario.getId_tipo_usuario()}" id="nombre" type="text" class="form-control" />

                        <label for="correo" class="control-label">Correo: </label>
                        <input name="correo" value="${usuario.getCorreo()}" id="nombre" type="text" class="form-control" />

                        <label for="pass" class="control-label">Contrase�a: </label>
                        <input name="pass" value="${usuario.getPass()}" id="nombre" type="password" class="form-control" />


                    </div>
                    <div class="form-group">
                        <input class="btn btn-primary pull-right" type="submit" value="Guardar" />
                    </div>
                </form>  
            </c:if> 
        </div>
    </div>
</div>
<jsp:include page="../shared/footer.jsp" /> 