<%-- 
    Document   : index
    Created on : Nov 11, 2017, 10:40:24 AM
    Author     : Gibran
--%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<jsp:include page="../shared/header.jsp" /> 

    <div class="container" >
        <div class="row">
            
            
            <div class="col-md-12">
                    <table class="table table-hover">
                        <thead>
                            <th>NIT</th>
                            <th>Transaccion</th>
                        </thead>
                        <tbody>
                            <c:forEach var="item" items="${list}">
                               <tr>
                                    <td>${item.get("nit")}</td>
                                    <td>${item.get("id_transaccion")}</td>
                                    <td>
                                        <a href="/buenoparati/transaccion/${item.get("id_transaccion")}" class="btn btn-warning btn-sm">Detalle</a>
                                    </td>
                                </tr> 
                            </c:forEach>
                            
                        </tbody>
                    </table>
                    
                    <c:if test="${list.size() == 0}">
                         <div class="alert alert-warning">
                            No Existen Registros
                        </div>
                    </c:if>
                   
                </div>
            
            
            
            
        </div>
        
    </div>

<jsp:include page="../shared/footer.jsp" /> 


