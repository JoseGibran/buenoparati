<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<jsp:include page="../shared/header.jsp" /> 

    <div class="container">
        <div class="col-md-12">
                    <table class="table table-hover">
                        <thead>
                            <th>Producto</th>
                        </thead>
                        <tbody>
                            <c:forEach var="item" items="${list}">
                               <tr>
                                    <td>${item.get("producto")}</td>
                                </tr> 
                            </c:forEach>
                            
                        </tbody>
                    </table>
                    
                    <c:if test="${list.size() == 0}">
                         <div class="alert alert-warning">
                            No Existen Registros
                        </div>
                    </c:if>
                   
                </div>
    </div>

<jsp:include page="../shared/footer.jsp" /> 
